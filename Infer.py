import cPickle
import DataRip
import numpy as np
#from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics.pairwise import cosine_similarity
from matplotlib import pyplot as plt

print "Loading Regression Model and Random projector.."
fhandle = open("RRegressionTrad.pkl", "rb")
RRegress = cPickle.load(fhandle)
fhandle.close()
fhandle = open("RandomProjector.pkl", 'rb')
grp = cPickle.load(fhandle)
fhandle.close()
print "Loading complete."
Signatures = DataRip.signRip("../Data/Animals_with_Attributes/Features/decaf/Def/")
XSign, YSign, XSignT, YSignT = [], [], [], []
print len(Signatures)
f = open("../Data/Animals_with_Attributes/Features/decaf/Def/testclasses.txt")
TestClasses = f.read()
#print TestClasses
TestClasses = TestClasses.split()
for sign in Signatures:
    if(sign[0] in TestClasses):
        XSign.append(sign[1])
        YSign.append(sign[0])
'''    else:
        XSignT.append(sign[1])
        YSignT.append(sign[0])
'''
XSign = XSignT + XSign
YSign = YSignT + YSign
print YSign
Frames = DataRip.classRip("../Data/Animals_with_Attributes/Features/decaf/Test/")
XTest = Frames["Features"].values
YTest = Frames["Class"].values
XTest = XTest.tolist()
YTest = YTest.tolist()
print "Starting Random projection.."
XTest = grp.transform(XTest)
#print len(XTest[0])
print "Dimention reduced to:", len(XTest[0]), "\nMapping and NN search.."
#W, X = np.matrix(RRegress.coef_), np.mat(XTest)
#print W.shape
#Maps = W*X.transpose()
Maps = RRegress.predict(XTest)
SimScore = cosine_similarity(Maps, XSign)
#print Maps.shape
#print YSign, "\n", simsco
PredictionsSim = []
tr = len(XSignT)
addScore = np.array([0]*len(XSign))
Differ = []
for oneScore in SimScore:
    lst = [(v, i) for i, v in enumerate(oneScore)]
    lar = max(lst)
    lst[lar[1]] = (0, lar[1])
    seclar = max(lst)
    diff = lar[0] - seclar[0]
    Differ.append(diff)
    PredictionsSim.append(YSign[max( (v, i) for i, v in enumerate(oneScore) )[1]])
    addScore = addScore + oneScore
KNN = KNeighborsClassifier(n_neighbors=1)
KNN.fit(XSign, YSign)
#print Maps.shape
predictions =  KNN.predict(Maps)
print "Accuracy:", accuracy_score(YTest, predictions), "\nAccu:", accuracy_score(YTest, PredictionsSim)
#print zip(YTest, predictions)
#print KNN.kneighbors_graph(Maps, n_neighbors = 5)
#print SimScore[0:5], YTest[0:5], PredictionsSim[0:5], Differ[0:5]
totC, totW, C, W = 0, 0, 0, 0
for lab in enumerate(YTest):
    if PredictionsSim[lab[0]] == lab[1]:
        totC += Differ[lab[0]]
        C += 1.0
    else:
        W += 1.0
        totW += Differ[lab[0]]
addScore = addScore / len(SimScore)
print "Correct:",totC/float(C), "\nWrong:",totW/float(W)
plt.bar(np.arange(tr), addScore[0:tr], color = "Black" )
plt.bar(np.arange(tr, len(XSign), 1), addScore[tr: len(XSign)], color = "Gray" )
plt.show()
