import os
import glob
from pandas import DataFrame

def blindRip(path):
    Files = glob.glob(path+"/*.txt")
    for File in Files:
        fhandle = open(File, 'r')
        VectX = fhandle.readlines()
        fhandle.close()
        Lines = []
        for line in VectX:
            try:
                Lines.append(float(line))
            except:
                print "Non-Float Value Read."
                Lines.append(line)
        yield Lines
def classRip(path):
    Frames = DataFrame({"Features": [], "Class": []})
    Classes = os.listdir(path)
    c = 0
    for oneClass in Classes:
        rows = []
        for Vect in blindRip(path+oneClass):
            rows.append({"Features": Vect, "Class": oneClass})
        frames = DataFrame(rows)
#        frames = frames.reindex(numpy.random.permutation(frames.index))
        Frames = Frames.append(frames)
        c += 1
        print c, oneClass, "scanned."
#    print Frames["Class"].values
    return Frames 

def signRip(path):
    ClassesList = []
    print "Scanning Signatures.."
    fhandle = open(path + "/classes.txt", 'r')
    RawLines = fhandle.readlines()
    for line in RawLines:
        info = tuple(line.split())
        ClassesList.append(info)
    fhandle = open(path + "/predicate-matrix-binary.txt")
    RawLines = fhandle.readlines()
    Signatures = []
    for Vect in RawLines:
        Vect = Vect.split()
        SigVect = [float(x) for x in Vect]
        Signatures.append((ClassesList.pop(0)[1], SigVect))
    return Signatures

#blindRip("Data/Animals_with_Attributes/Features/decaf/bat/")
#classRip("Data/Animals_with_Attributes/Features/decaf/")
#signRip("Data/Animals_with_Attributes/Features/Def")
