import DataRip as DR
from sklearn.svm import LinearSVC
from sklearn.linear_model import Ridge
#from sklearn.linear_model import LinearRegression
from sklearn.random_projection import GaussianRandomProjection
from sklearn.svm import SVR
import numpy as np
import cPickle

print "Initiating Launch.."
Frames = DR.classRip("../Data/Animals_with_Attributes/Features/decaf/Train/")
Signatures = DR.signRip("../Data/Animals_with_Attributes/Features/decaf/Def/")
print "Data and Signatures Scanned."
XTrain = Frames["Features"].values
YTrain = Frames["Class"].values
XTrain = XTrain.tolist()
YTrain = YTrain.tolist()
print "Sample Scanned:", len(XTrain)
print "Starting Random projection.."
grp = GaussianRandomProjection(n_components = 500)
grp.fit(XTrain)
XTrain = grp.transform(XTrain)
print "Dimention reduced to:", len(XTrain[0])
#print len(XTrain[1])

fhandle = open("RandomProjector.pkl",'wb')
cPickle.dump(grp, fhandle)
fhandle.close()

YSig = []
for Y in YTrain:
    for s in Signatures:
        if s[0] == Y:
            YSig.append(s[1])
            break;

print "Fitting model.."
Classifiers = []
for i in range(0, len(YSig[0])):
    svc = SVR()
    YAttr = [x[i] for x in YSig]
    svc.fit(XTrain, YAttr)
    Classifiers.append(svc)

#ridge =  Ridge()
#linear = LinearRegression()
#ridge.fit(XTrain, YSig)
#ridge.predict( )
#print len(ridge.coef_), len(ridge.coef_[0])
print "Fitting complete."

fhandle = open("SVCs.pkl", 'wb')
cPickle.dump(Classifiers, fhandle)
fhandle.close()
