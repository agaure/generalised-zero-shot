import DataRip as DR
from sklearn.svm import LinearSVC
from sklearn.linear_model import Ridge
#from sklearn.linear_model import LinearRegression
from sklearn.random_projection import GaussianRandomProjection
import numpy as np
import cPickle

print "Initiating Launch.."
Frames = DR.classRip("../Data/Animals_with_Attributes/Features/decaf/Train/")
Signatures = DR.signRip("../Data/Animals_with_Attributes/Features/decaf/Def/")
print "Data and Signatures Scanned."
XTrain = Frames["Features"].values
YTrain = Frames["Class"].values


XTrain = XTrain.tolist()
YTrain = YTrain.tolist()
print len(XTrain)
print "Starting Random projection.."
grp = GaussianRandomProjection(n_components = 500)
grp.fit(XTrain)
XTrain = grp.transform(XTrain)
print "Dimention reduced to:", len(XTrain[0])
#print len(XTrain[1])

fhandle = open("RandomProjector.pkl",'wb')
cPickle.dump(grp, fhandle)
fhandle.close()
'''
print len(Signatures)
z = zip(range(0, 50), Signatures)
#for r in z:
#    print r
Avg = [np.array([0]*500)]*40
#Avg = Avg.reshape((40, 500))
#print Avg.shape
c, cnt = [0.0]*40, 0
print len(YTrain), len(XTrain)
for x, y in zip(XTrain, YTrain):
    #c+=1
    for r in z:
        #print y , r[1][0]
        if r[1][0] == y:
            cnt += 1
            Avg[r[0]] = (Avg[r[0]] + np.array(x))
            #print (Avg[r[0]]) #.shape , Avg[0][0].shape, (np.matrix(x)).shape
            c[r[0]] += 1.0
GY = []
for i, ss in z:
    Avg[i] = (Avg[i]/c[i])
    #print i, ss
    GY.append(ss[1])
print Avg , c, GY, cnt #, ss, len(AL)

Avg, GSig = [], []
c = []
for sig in Signatures:
    temp = np.array([0]*500)
    f = False
    cnt = 0
    for i, x in enumerate(XTrain):
        if(YTrain[i] == sig[0]):
            f = True
            temp = temp + np.array(x)
            cnt += 1
    if(f):
        Avg.append(temp)
        GSig.append(sig[1])
        c.append(cnt)

for i, a in enumerate(Avg):
    Avg[i] = a/c[i]
#print Avg, GSig, c
'''
YSig = []
#XAvg = np.matrix([0]*85*40)
#XAvg.reshape((40, 85))
for i, Y in enumerate(YTrain):
    for s in Signatures:
        if s[0] == Y:
            YSig.append(s[1])
            break;
ridgeTrad = Ridge()
#ridgeGttr = Ridge()
#linear = LinearRegression()
print "Fitting model.."
ridgeTrad.fit(XTrain, YSig)
#ridgeGttr.fit(Avg, GSig)
#ridge.predict( )
#print len(ridge.coef_), len(ridge.coef_[0])
print "Fitting complete."

fhandle = open("RRegressionTrad.pkl", 'wb')
cPickle.dump(ridgeTrad, fhandle)
fhandle.close()
'''
fhandle = open("RRegressionGttr.pkl", 'wb')
cPickle.dump(ridgeGttr, fhandle)
fhandle.close()
'''
